import * as express from "express";
import * as cors from "cors";
import { createServer } from "http";

const PORT = process.env["PORT"] || 80;

const app = express();
app.set("port", PORT);
app.use(cors());

// routing
app.get("/", (req, res) => {
  res.write("Hello world");
  res.end();
});

const server = createServer(app);

server.listen(PORT, () => {
  console.log("server listen on port ", PORT);
});
